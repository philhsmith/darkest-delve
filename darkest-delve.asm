.case +                                     ; case-sensitive identifiers
.linecont -                                 ; no line continuations
.localchar '@'                              ; local labels start with @
.smart -                                    ; jackie chan doesn't want any trouble

.p02                                        ; 6502 (NES)

;;; I/O Registers

; PPU Control 1 (write only)
;
; 0-1 Nametable address (0: $2000, 1: $2400, 2: $2800, 3: $2C00)
;   2 PPU_ADDR increment per PPU_DATA access (0: 1, 1: 32)
;   3 Sprite pattern table address for 8x8 sprites (not 8x16) (0: $0000, 1: $1000)
;   4 Background pattern table address (0: $0000, 1: $1000)
;   5 Sprite height (0: 8x8, 1: 8x16)
;   6 Must be zero to avoid short circuit!
;   7 NMI enable (0: off, 1: on)
PPU_CONTROL := $2000

; PPU Control 2 (write only) (all: 0: off, 1: on)
;
;   0 Greyscale
;   1 Show background in leftmost 8 pixels of screen
;   2 Show sprites in leftmost 8 pixels of screen
;   3 Show background
;   4 Show sprites
;   5 Emphasize NTSC red
;   6 Emphasize NTSC green
;   7 Emphasize NTSC blue
PPU_MASK := $2001

; PPU Status (read only) (all: 0: false, 1: true)
;
;   5 Sprite overflow (buggy!)
;   6 Sprite 0 hit
;   7 Vertical blank has started
PPU_STATUS := $2002

PPU_SCROLL := $2005                         ; PPU Background Scroll Offset (write only)
PPU_ADDR := $2006                           ; PPU Address (write only)
PPU_DATA := $2007                           ; PPU I/O (read/write)

OAM_ADDR := $2003                           ; Sprite RAM Address (write only)
OAM_DATA := $2004                           ; Sprite RAM I/O (write only)
OAM_DMA := $4014                            ; Sprite DMA (write only)

;; iNES Header

.segment "HEADER"

  .byte "NES", $1A                          ; Magic numbers
  .byte 2                                   ; 16 KB PRG-ROM bank count
  .byte 1                                   ;  8 KB CHR-ROM bank count

;; Interrupt Handlers

.segment "VECTORS"

  .word nmi                                 ; Vertical Blank
  .word reset                               ; Reset button
  .word irq                                 ; Sound, MMCX scanline events

;; Graphics ROM

.segment "CHARS"

  .incbin "ascii.chr"                       ; A font for a subset of ascii

;; Program ROM

.segment "CODE"

irq:
.proc reset
  lda #$01                                  ; Beep
  sta $4015
  lda #$9F
  sta $4000
  lda #$22
  sta $4003

  lda #$80                                  ; Enable NMI
  sta PPU_CONTROL

@forever:
  jmp @forever
.endproc

.segment "CODE"

.proc nmi
  lda #$3F                                  ; Load a color palette
  sta PPU_ADDR
  lda #$00
  sta PPU_ADDR
  lda #$0F
  sta PPU_DATA
  lda #$20
  sta PPU_DATA
  lda #$20
  sta PPU_DATA
  lda #$20
  sta PPU_DATA

  lda #$21                                  ; Put some tiles on nametable 0
  sta PPU_ADDR
  lda #$CA
  sta PPU_ADDR

.pushseg
.segment "RODATA"
@darkest_delve:
  .asciiz "Darkest Delve"
.popseg

  ldx #$FF
@print_char:
  inx
  lda @darkest_delve, X
  sta PPU_DATA
  bne @print_char

  lda #$00                                  ; Set scroll position to upper left
  sta PPU_SCROLL
  sta PPU_SCROLL

  lda #$1E                                  ; Enable sprites and background
  sta PPU_MASK

  rti
.endproc
