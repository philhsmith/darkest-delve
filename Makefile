# vim: filetype=make

SHELL := /bin/bash

DOCKER := docker run \
  --volume '${PWD}:/darkest-delve' \
  --workdir /darkest-delve \
  --interactive --tty \
  --name darkest-delve \
  --rm \
  philhsmith/cc65:V2.16-alpine \

ifneq ($(shell which ca65),)
CA65 := ca65
LD65 := ld65
DA65 := da65
else
CA65 := ${DOCKER} ca65
LD65 := ${DOCKER} ld65
DA65 := ${DOCKER} da65
endif

.PHONY: all
all: build/darkest-delve.nes

.PHONY: test
test:

.PHONY: continuous-integration
continuous-integration: build/darkest-delve.nes

.PHONY: clean
clean:
	-rm -rf build

.PHONY: run
run: build/darkest-delve.nes
	nesemu2 $<

build:
	mkdir -p build

build/%.o: %.asm | build
	${CA65} $< -o $@

build/darkest-delve.nes: memory-layout build/darkest-delve.o | build
	${LD65} -C memory-layout build/darkest-delve.o -o $@

build/%-disassembled.asm: build/%.nes | build
	${DA65} --info disassembler.info $< -o $@
